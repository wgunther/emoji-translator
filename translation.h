#ifndef EMOJI_TRANSLATOR_TRANSLATION_H_
#define EMOJI_TRANSLATOR_TRANSLATION_H_

#include <ostream>
#include <string>
#include <vector>

#include "dictionary.h"

namespace emoji_translator {

std::string Translate(const std::vector<std::string>& token_list,
                      const Dictionary& dict,
                      std::ostream* log_stream = nullptr);

}  // namespace emoji_translator

#endif  // EMOJI_TRANSLATOR_TRANSLATION_H_
