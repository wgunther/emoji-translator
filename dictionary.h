#ifndef EMOJI_TRANSLATOR_DICTIONARY_H_
#define EMOJI_TRANSLATOR_DICTIONARY_H_

#include <limits>
#include <map>
#include <ostream>
#include <string>
#include <vector>

namespace emoji_translator {

class TrieNode {
 public:
  TrieNode() : score_{std::numeric_limits<double>::lowest()} {}

  template <typename ForwardIterator1, typename ForwardIterator2>
  void AddTokenList(
      ForwardIterator1 token_list_begin, ForwardIterator1 token_list_end,
      double score, ForwardIterator2 translations_begin,
      ForwardIterator2 translations_end);

  const TrieNode* FindChildOrNull(const std::string& token) const;

  bool empty() const { return score_ == std::numeric_limits<double>::lowest(); }
  double score() const { return score_; }
  const std::vector<std::string>& translations() const { return translations_; }

 private:
  double score_;
  std::vector<std::string> translations_;
  std::map<std::string, TrieNode> children_;
};

class Dictionary {
 public:
  void ReadFromFile(const std::string& filename,
                    std::ostream* log_stream = nullptr);

  void ParseLine(const std::string& line,
                 std::ostream* log_stream = nullptr);

  const TrieNode& root() const { return root_; }

 private:
  TrieNode root_;
};

// Implementation of template function.

template <typename ForwardIterator1, typename ForwardIterator2>
void TrieNode::AddTokenList(
    ForwardIterator1 token_list_begin, ForwardIterator1 token_list_end,
    double score, ForwardIterator2 translations_begin,
    ForwardIterator2 translations_end) {
  if (token_list_begin == token_list_end) {
    if (score > score_) {
      score_ = score;
      translations_.clear();
      translations_.insert(translations_.end(), translations_begin,
                           translations_end);
    } else if (score == score_) {
      translations_.insert(translations_.end(), translations_begin,
                           translations_end);
    }
  } else {
    TrieNode& child = children_[*token_list_begin];
    ++token_list_begin;
    child.AddTokenList(token_list_begin, token_list_end, score,
                       translations_begin, translations_end);
  }
}

}  // namespace emoji_translator

#endif  // EMOJI_TRANSLATOR_DICTIONARY_H_
