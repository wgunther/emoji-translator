#include "dictionary.h"

#include <cstddef>
#include <deque>
#include <fstream>
#include <ostream>
#include <string>

namespace emoji_translator {

const TrieNode* TrieNode::FindChildOrNull(const std::string& token) const {
  const auto child_iter = children_.find(token);
  if (child_iter == children_.end())
    return nullptr;
  else
    return &child_iter->second;
}

void Dictionary::ReadFromFile(const std::string& filename,
                              std::ostream* log_stream) {
  std::ifstream in(filename);
  std::string line;
  while (std::getline(in, line)) {
    ParseLine(line, log_stream);
  }
}

static std::deque<std::string> SplitOnWhitespace(const std::string& s,
                                                 const std::size_t start_pos,
                                                 const std::size_t length) {
  std::deque<std::string> tokens;
  std::size_t token_start = start_pos;
  bool reading_whitespace = true;
  std::size_t pos = start_pos;
  for (; pos < start_pos + length; ++pos) {
    switch (s[pos]) {
      case ' ':
      case '\t':
      case '\n':
      case '\r':
        if (!reading_whitespace) {
          tokens.push_back(s.substr(token_start, pos - token_start));
          reading_whitespace = true;
        }
        break;
      default:
        if (reading_whitespace) {
          token_start = pos;
          reading_whitespace = false;
        }
        break;
    }
  }
  if (!reading_whitespace)
    tokens.push_back(s.substr(token_start, pos - token_start));
  return tokens;
}

void Dictionary::ParseLine(const std::string& line, std::ostream* log_stream) {
  const std::size_t separator_pos = line.find_first_of('|');
  if (separator_pos == std::string::npos) return;
  std::deque<std::string> token_list =
      SplitOnWhitespace(line, 0, separator_pos);
  if (token_list.empty()) return;
  std::deque<std::string> translations = SplitOnWhitespace(
      line, separator_pos + 1, line.length() - separator_pos - 1);
  double score = 0.0;
  if (!translations.empty()) {
    try {
      std::size_t pos_after_double = 0;
      score = std::stod(translations[0], &pos_after_double);
      if (pos_after_double == translations[0].length()) {
        translations.pop_front();
      } else {
        score = 0.0;
      }
    } catch (std::invalid_argument) {
      score = 0.0;
    } catch (std::out_of_range) {
      score = 0.0;
    }
  }

  if (log_stream != nullptr) {
    *log_stream << "Adding token list [\"";
    auto token_iter = token_list.begin();
    for (;;) {
      *log_stream << *token_iter;
      if (++token_iter == token_list.end()) break;
      *log_stream << "\", \"";
    }
    *log_stream << "\"] with score " << score << " and translations [";
    if (!translations.empty()) {
      *log_stream << "\"";
      auto translation_iter = translations.begin();
      for (;;) {
        *log_stream << *translation_iter;
        if (++translation_iter == translations.end()) break;
        *log_stream << "\", \"";
      }
      *log_stream << "\"";
    }
    *log_stream << "].\n";
  }

  root_.AddTokenList(token_list.begin(), token_list.end(), score,
                     translations.begin(), translations.end());
}

}  // namespace emoji_translator
