#include <algorithm>
#include <iostream>
#include <iterator>
#include <sstream>
#include <string>
#include <vector>

#include "dictionary.h"
#include "translation.h"

int main(int argc, char** argv) {
  emoji_translator::Dictionary dict;
  for (int i = 1; i < argc; ++i) {
    dict.ReadFromFile(argv[i]);
  }
  std::string line;
  while (std::getline(std::cin, line)) {
    std::vector<std::string> token_list;
    std::istringstream iss(line);
    std::copy(std::istream_iterator<std::string>(iss),
              std::istream_iterator<std::string>(),
              std::back_inserter(token_list));
    std::cout << emoji_translator::Translate(token_list, dict, &std::cerr)
              << std::endl;
  }
  return 0;
}
