CXXFLAGS += -std=c++11 -Wall -Wextra -pedantic -O3

emoji_translator : main.cc dictionary.o translation.o
	$(CXX) $(CXXFLAGS) -o emoji_translator main.cc dictionary.o translation.o

dictionary.o : dictionary.h dictionary.cc
translation.o : translation.h translation.cc dictionary.h
