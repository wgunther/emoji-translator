#include "translation.h"

#include <cstddef>
#include <deque>
#include <limits>
#include <ostream>
#include <random>
#include <string>
#include <utility>
#include <vector>

#include "dictionary.h"

namespace emoji_translator {

constexpr double kUntranslatableTokenScore{-2.0};

// A TranslationPiece object represents a translation of a sequence of
// consecutive tokens in a token list, as a sequence of pointers to strings in
// a Dictionary (or the token list, for untranslatable tokens). A
// TranslationPiece object is intended to be temporary (i.e., it should not
// outlive the Dictionary or token list to which its elements point) and does
// not take ownership of the pointers.
using TranslationPiece = std::vector<const std::string*>;

// A tail translation is a translation of a final sequence of a token list.
using BestTails = std::deque<std::pair<double, TranslationPiece>>;

// Precondition: best_tails must point to a nonempty BestTails object. For the
// first call to ComputeNextTail, populate best_tails with a single element
// having score 0.0 and an empty TranslationPiece, representing the best tail
// translation of the final empty sequence of the token list.
static void ComputeNextTail(const std::vector<std::string>& token_list,
                            const Dictionary& dict, BestTails* best_tails) {
  static std::default_random_engine random_engine;
  static const std::string space{" "};
  // Track the best score and translation prefix and suffix seen so far.
  double best_score = std::numeric_limits<double>::lowest();
  TranslationPiece best_prefix;
  const TranslationPiece* best_suffix = nullptr;
  // Offset of the first token in the token sequences we are considering.
  const auto first_token_pos = token_list.size() - best_tails->size();
  auto tail_iter = best_tails->begin();      // Iterator through the tails.
  const TrieNode* trie_node = &dict.root();  // Pointer to current node of trie.
  for (auto token_pos = first_token_pos; token_pos < token_list.size();
       ++token_pos, ++tail_iter) {
    trie_node = trie_node->FindChildOrNull(token_list[token_pos]);
    if (trie_node == nullptr) break;   // End of this branch of the trie.
    if (trie_node->empty()) continue;  // Nothing at this node.
    double score = tail_iter->first + trie_node->score();
    if (score >= best_score) {
      best_score = score;
      auto num_translations = trie_node->translations().size();
      if (num_translations == 0) {
        best_prefix.clear();
      } else if (num_translations == 1) {
        best_prefix.assign({&trie_node->translations()[0]});
      } else {
        // Choose translation at random.
        std::uniform_int_distribution<std::vector<std::string>::size_type>
            dist{0, num_translations - 1};
        best_prefix.assign({&trie_node->translations()[dist(random_engine)]});
      }
      best_suffix = &tail_iter->second;
    }
  }
  if (best_suffix == nullptr) {
    // If we didn't find anything, this is an untranslatable token.
    best_score = best_tails->front().first + kUntranslatableTokenScore;
    // TODO: Fix spacing.
    best_prefix.assign({&space, &token_list[first_token_pos], &space});
    best_suffix = &best_tails->front().second;
  }
  best_tails->emplace_front(best_score, best_prefix);
  auto& tail = best_tails->front().second;
  tail.insert(tail.end(), best_suffix->begin(), best_suffix->end());
}

static std::string TranslationPieceToString(const TranslationPiece& tail) {
  std::size_t length = 0;
  for (const std::string* s : tail) length += s->length();
  std::string result;
  result.reserve(length);
  for (const std::string* s : tail) result += *s;
  return result;
}

std::string Translate(const std::vector<std::string>& token_list,
                      const Dictionary& dict, std::ostream* log_stream) {
  if (token_list.empty()) return std::string();
  BestTails best_tails{{0.0, TranslationPiece()}};
  while (best_tails.size() <= token_list.size()) {
    ComputeNextTail(token_list, dict, &best_tails);
    if (log_stream != nullptr) {
      *log_stream << "Best translation of \"";
      for (auto token_pos = token_list.size() - best_tails.size() + 1; ;) {
        *log_stream << token_list[token_pos];
        if (++token_pos == token_list.size()) break;
        *log_stream << ' ';
      }
      *log_stream << "\" is \""
                  << TranslationPieceToString(best_tails.front().second)
                  << "\" with score " << best_tails.front().first << ".\n";
    }
  }
  return TranslationPieceToString(best_tails.front().second);
}

}  // namespace emoji_translator
