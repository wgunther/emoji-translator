var viewModel = {
	emojiData: ko.observable(null),
	fileError: ko.observable(""),
	whichChoosing: ko.observable(""),
	searchString: ko.observable(""),
	displayedResults: ko.observableArray(),
	lastSearched: ko.observable(""),
	newWord: ko.observable(""),
	newString: ko.observable(""),
	newScore: ko.observable(""),
	serializedData: ko.observable(""),
	choosingKind: { emoji: "emoji", word: "word" },

	onFileChange: function (data, event) {
		var file = event.target.files[0];
		if (file) {
			this.validateFile(file);
		} else {
			this.emojiData(null);
			this.whichChoosing("");
		}
	},

	validateFile: function (file) {
		var fileReader = new FileReader();
		fileReader.onerror = this.fileError.bind(this, "Error reading file")
		fileReader.onload = this.parseFileResult.bind(this, fileReader);
		fileReader.readAsText(file);
	},

	parseFileResult: function (fileReader) {
		var fileContents = fileReader.result;
		this.parseString(fileContents);
	},

	parseString: function(fileContents) {
		var emojiData = new EmojiData();
		var lines = fileContents.split("\n");
		try {
			for (var i = 0; i < lines.length - 1; ++i) {
				emojiData.parseLine(lines[i]);
			}
		}
		catch (exception) {
			this.fileError(exception.toString());
			return;
		}

		this.emojiData(emojiData);
		this.serialize();
	},

	onSearchChange: function () {
		this.resetNewRow();
		switch (this.whichChoosing()) {
			case this.choosingKind.emoji:
				this.searchEmoji();
				break;
			case this.choosingKind.word:
				this.searchWord();
				break;
		}
		this.lastSearched(this.searchString());
	},


	onScoreChange: function () {
		this.emojiData().updateScore(this.searchString(), this.newScore());
	},

	resetNewRow: function() {
		switch (this.whichChoosing()) {
			case this.choosingKind.emoji:
				this.newWord("");
				this.newString(this.searchString());
				break;
			case this.choosingKind.word:
				this.newString("");
				this.newWord(this.searchString());
				break;
		}
		this.newScore(this.emojiData().getScore(this.searchString()));
	},

	searchEmoji: function() {
		var results = this.emojiData().lookupEmoji(this.searchString());
		this.displayedResults(results.concat([]));
		if (results.length === 0) {
			console.log("None found");
		}
	},

	searchWord: function () {
		var results = this.emojiData().lookup(this.searchString());
		this.displayedResults(results.concat([]));
		if (results.length === 0) {
			console.log("None found");
		}
	},

	checkNewValue: function () {
		if (this.newString() !== "" && this.newScore() !== "" && !isNaN(parseInt(this.newScore())) && this.newWord() !== "") {
			this.addNewValue();
		}
	},

	addNewValue: function () {
		var score = new EmojiScore(this.newWord(), this.newString(), this.newScore());
		this.displayedResults.push(score);

		// data integrity
		this.emojiData().lookup(this.newWord()).push(score);
		this.emojiData().lookupEmoji(this.newString()).push(score);

		//reset
		this.resetNewRow();
	},

	onSaveChanges: function () {
		this.serialize();
		this.parseString(this.serializedData());
		this.onSearchChange();
	},

	serialize: function () {
		var serialized = this.emojiData().serialize();
		this.serializedData(serialized);
	}
}

ko.applyBindings(viewModel);

viewModel.newScore.subscribe(viewModel.checkNewValue, viewModel);
viewModel.newString.subscribe(viewModel.checkNewValue, viewModel);
viewModel.newWord.subscribe(viewModel.checkNewValue, viewModel);

/*
	EmojiData
*/

function EmojiData() {
	this.dictionary = {};
	this.emojiDictionary = {};
}
EmojiData.prototype = {
	dictionary: null,
	emojiDictionary: null,

	parseLine: function (line) {
		var data = line.split(" | ");
		if (!data.length === 2) {
			throw new EmojiData.ParseException("Bad line: " + line);
		}

		var word = data[0].trim();
		var emojiString = data[1].trim();

		if (!word) {
			throw new EmojiData.ParseException("Bad word: " + line);
		}

		if (!emojiString) {
			throw new EmojiData.ParseException("Bad emoji: " + line);
		}

		var scores = EmojiScore.parseLine(word, emojiString);

		this.cacheScores(scores);
	},

	cacheScores: function(scores) {
		for (var i = 0; i < scores.length; ++i) {
			var score = scores[i];

			var word = score.word();
			var emoji = score.string();

			this.dictionary[word] = this.dictionary[word] || [];
			this.dictionary[word].push(score);

			this.emojiDictionary[emoji] = this.emojiDictionary[emoji] || [];
			this.emojiDictionary[emoji].push(score);
		}
	},

	updateScore: function(word, score) {
		var lookup = this.lookup(word);
		for (var i = 0; i < lookup.length; ++i) {
			lookup[i].score(score);
		}
	},
	
	getScore: function(word) {
		var lookup = this.lookup(word);
		if (lookup.length > 0) {
			return lookup[0].score();
		}
		return 0;
	},

	lookup: function (word) {
		if (!this.dictionary[word]) {
			this.dictionary[word] = [];
		}
		return this.dictionary[word];
	},

	lookupEmoji: function (emojiString) {
		if (!this.emojiDictionary[emojiString]) {
			this.emojiDictionary[emojiString] = [];
		}
		return this.emojiDictionary[emojiString];
	},

	addValue: function (word, emojiScore) {
		if (this.dictionary[word]) {
			this.dictionary[word].push(emojiScore);
		} else {
			this.dictionary[word] = [emojiScore];
		}
	},

	contract: function (word) {
		var values = this.lookup(word);
		for (var i = values.length - 1; i >= 0; --i) {
			if (values[i].isEmpty()) {
				values.splice(i, 1);
			}
		}
	},

	serialize: function () {
		var string = "";
		var keys = Object.keys(this.dictionary).sort();
		for (var i = 0; i < keys.length; ++i) {
			var word = keys[i];
			if (this.dictionary[word].length === 0) {
				continue;
			}

			var scoreArray = this.dictionary[word];
			var serializedEmoji = EmojiScore.serialize(scoreArray);
			if (serializedEmoji) {
				string += word + " | " + serializedEmoji + "\n";
			}
		}
		return string;
	}
}

EmojiData.ParseException = function (info) {
	this.data = info;
}

EmojiData.ParseException.prototype = {
	toString: function () { return this.data; }
}

/*
	EmojiScore
*/

EmojiScore = function (word, string, score) {
	this.word = ko.observable(word);
	this.string = ko.observable(string);
	this.score = ko.observable(score);
}

EmojiScore.prototype = {
	word: null,
	score: null,
	string: null,

	serialize: function () {
		if (!this.word() || !this.string()) {
			return;
		}
		return this.string();
	},

	isEmpty: function () {
		return !this.string();
	}
}

EmojiScore.parseLine = function (word, line) {
	var emojis = line.split(" ");
	var ret = [];

	var scoreModifier = EmojiScore.parseScore(emojis[0]);
	if (isNaN(scoreModifier)) {
		scoreModifier = 0;
	} else {
		emojis.shift();
	}


	if (emojis.length === 0) {
		ret.push(new EmojiScore(word, "", scoreModifier));
	} else {
		for (var i = 0; i < emojis.length; ++i) {
			ret.push(new EmojiScore(word, emojis[i], scoreModifier));
		}
	}


	return ret;
}

EmojiScore.serialize = function (scoreArray) {

	if (!scoreArray || scoreArray.length === 0) {
		return "";
	}

	var string = "";
	var score = scoreArray[0].score();
	if (score !== 0) {
		string = score;
	}

	for (var i = 0; i < scoreArray.length; ++i) {
		var serialized = scoreArray[i].serialize();
		if (!serialized) {
			continue;
		}
		if (string !== "") {
			string += " ";
		}
		string += serialized
	}
	return string;
}

EmojiScore.EmojiScoreException = function (info) {
	this.data = info;
}

EmojiScore.EmojiScoreException.prototype = {
	toString: function () { return this.data; }
}

EmojiScore.parseScore = function (score) {
	var scoreValue = parseFloat(score);
	return scoreValue;
}
